﻿using System.ComponentModel;

namespace Task7
{
	public abstract class Animal
	{
		public string Species { get; set; }
		public int Height { get; set; }
		public string EyeColor { get; set; }
		public char Gender { get; set; }

		public Animal(string species, int height, string eyeColor, char Gender)
		{
			Species = species;
			Height = height;
			EyeColor = eyeColor;
			this.Gender = Gender;
		}

		public override string ToString()
		{
			return "Species: " + this.Species + ", Height: " + this.Height + "cm, eye color: " + this.EyeColor + ", gender: " + this.Gender;
		}

		public abstract void Eat();
		public abstract void Sleep();
	}
}

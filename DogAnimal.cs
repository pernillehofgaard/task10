﻿namespace Task7
{
    public class DogAnimal : Animal
    {

        public int years { get; set; }

        public DogAnimal(string species, int height, string eyeColor, char Gender, int years)
            : base(species, height, eyeColor, Gender)
        {
            this.years = years;
        }

        public override string ToString()
        {
            return base.ToString() + ", Years: " + this.years;
        }

        public override void Eat()
        {
            System.Console.WriteLine(this.Species + " is eating");
        }

        public override void Sleep()
        {
            System.Console.WriteLine(this.Species + " is sleeping");
        }
    }





}

﻿namespace Task7
{
	public class CatAnimal : Animal
	{
		public double HoursSinceLastMeal { get; set; }

		public CatAnimal(string species, int height, string eyeColor, char Gender, double HoursSinceLastMeal)
			:base(species, height, eyeColor, Gender)
		{
			this.HoursSinceLastMeal = HoursSinceLastMeal;
		}

		public virtual void Meow() 
		{
			string wichGender = "";
			if (Gender.Equals('F'))
			{
				wichGender = "she";
			}
			else
			{
				wichGender = "he";
			}

			System.Console.WriteLine(this.Species + " is meowing because it has been " + this.HoursSinceLastMeal + " hours since " + wichGender + " ate");
		}

		public override string ToString()
		{
			return base.ToString();
		}

		public override void Eat()
		{
			System.Console.WriteLine(this.Species + " is eating");
		}

		public override void Sleep()
		{
			System.Console.WriteLine(this.Species + " is sleeping");
		}
	}
}

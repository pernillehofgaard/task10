﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task7
{
	class Program
	{

		static void Main(string[] args)
		{
			List<Animal> animals = new List<Animal>();

			HouseCat cat = new HouseCat("Main Coone", 40, "Green", 'M', 0.1, "Garfield");
			HouseCat anotherCat = new HouseCat("Rag doll", 25, "Blue", 'F', 1.5, "Ulthar");
			HouseCat cat3 = new HouseCat("Main Coone", 10, "Blue", 'M', 1.7, "Simba");

			CatAnimal tiger = new CatAnimal("Tiger", 100, "Yellow", 'M', 2.0);
			CatAnimal lion = new CatAnimal("Lion", 120, "Yellow", 'M', 5.0);

			DogAnimal wolf = new DogAnimal("Gray Wolf", 100, "Gray",'M', 9);
			DogAnimal dog = new DogAnimal("Dachshund", 40, "Brown",'F', 4);

			BirdAnimal hawk = new BirdAnimal("Hawk", 50, "Brown",'M', 100);
			BirdAnimal trashDow = new BirdAnimal("Dow", 20, "Black",'F', 40);

			animals.Add(cat);
			animals.Add(anotherCat);
			animals.Add(cat3);
			animals.Add(tiger);
			animals.Add(lion);

			animals.Add(wolf);
			animals.Add(dog);

			animals.Add(hawk);
			animals.Add(trashDow);

			/*
			Console.WriteLine(); //make it look nicer in the console
			cat.Meow();
			tiger.Meow();
			dog.Sleep();
			wolf.Eat();
			*/


			Console.WriteLine("what would you like to do?");
			bool keepGoing = true;
			while (keepGoing)
			{
				Console.WriteLine("1- See collection of animals \n" +
								  "2 - Quit");
				int answer = Convert.ToInt32(Console.ReadLine());

				switch (answer)
				{
					case 1:
						//see collection
						Console.WriteLine("all, bird, cat, dog?");
						string collection = Console.ReadLine();

						if (collection.ToUpper().Equals("ALL"))
						{
							IEnumerable<Animal> res = (from Animal in animals
													   select Animal);

							foreach (Animal allAnimals in res)
							{
								Console.WriteLine(allAnimals);
							}

							Console.WriteLine("\nsort by: height, gender, species");
							string choice = Console.ReadLine();

							if(choice.ToUpper().Equals("HEIGHT"))
							{
								Console.WriteLine("Enter minimum height: ");
								int minimumHeight = Convert.ToInt32(Console.ReadLine());

								IEnumerable<Animal> result = from Animal in animals
															 where Animal.Height >= minimumHeight
															 select Animal;

								foreach(var heightCriteria in result)
								{
									Console.WriteLine(heightCriteria);
								}
							}
							else if (choice.ToUpper().Equals("GENDER"))
							{
								Console.WriteLine("Enter gender (M/F):" );
								char genderLetter = Convert.ToChar(Console.ReadLine().ToUpper());

								IEnumerable<Animal> genderAnimal = from Animal in animals
																   where Animal.Gender == genderLetter
																   select Animal;

								foreach(var genderCriteria in genderAnimal)
								{
									Console.WriteLine(genderCriteria);
								}
							}
							else
							{
								//species
								Console.WriteLine("Enter species: ");
								string species = Console.ReadLine().ToUpper();

								IEnumerable<Animal> speciesCriteria = from Animal in animals
																	  where Animal.Species.ToUpper() == species
																	  select Animal;

								foreach(Animal speciesAnimal in speciesCriteria)
								{
									Console.WriteLine(speciesAnimal);
								}
							}
						}

						else if (collection.ToUpper().Equals("CAT"))
						{
							try
							{
								var catAnimals = animals.OfType<CatAnimal>();
																		
								foreach (CatAnimal cats in catAnimals)
								{
									Console.WriteLine(cats);
								}

								Console.WriteLine("\nSort by: height, gender, species: ");
								string catSort = Console.ReadLine().ToUpper();

								if(catSort == "HEIGHT")
								{
									Console.WriteLine("Enter minimum height: ");
									int minimumHeightCat = Convert.ToInt32(Console.ReadLine());

									IEnumerable<Animal> catHeigtCriteria = from Cat in animals.OfType<CatAnimal>()
																		   where Cat.Height >= minimumHeightCat
																		   select Cat;

									foreach(CatAnimal catHeight in catHeigtCriteria)
									{
										Console.WriteLine(catHeight);
									}
								}else if(catSort == "GENDER")
								{
									Console.WriteLine("Enter gender (M/F): ");
									char genderCat = Convert.ToChar(Console.ReadLine().ToUpper());

									IEnumerable<Animal> genderCriteriaCat = from Cat in animals.OfType<CatAnimal>()
																			where Cat.Gender == genderCat
																			select Cat;

									foreach(CatAnimal catGender in genderCriteriaCat)
									{
										Console.WriteLine(catGender);
									}
								}
								else
								{
									//Species
									Console.WriteLine("Enter species: ");
									string catSpecies = Console.ReadLine().ToUpper();

									IEnumerable < Animal > CatSpecies = from Cat in animals.OfType<CatAnimal>()
																		where Cat.Species.ToUpper() == catSpecies
																		select Cat;

									foreach(CatAnimal catSpeciesMet in CatSpecies)
									{
										Console.WriteLine(catSpeciesMet);
									}
								}
							}
							catch (Exception e)
							{
								Console.WriteLine(e.Message);
							}
							
						}
						else if (collection.ToUpper().Equals("DOG"))
						{
							var dogAnimals= animals.OfType<DogAnimal>();

							foreach (DogAnimal dogs in dogAnimals)
							{
								Console.WriteLine(dogs);
							}

							Console.WriteLine("\nSort by: height, gender, species: ");
							string dogSort = Console.ReadLine().ToUpper();

							if (dogSort.Equals("HEIGHT"))
							{
								Console.WriteLine("Enter minimum height: ");
								int dogHeight = Convert.ToInt32(Console.ReadLine());

								IEnumerable<Animal> dogHeightCriteria = from Dog in animals.OfType<DogAnimal>()
																		where Dog.Height >= dogHeight
																		select Dog;
								foreach(DogAnimal dogHeightCriteriaMet in dogHeightCriteria)
								{
									Console.WriteLine(dogHeightCriteriaMet);
								}
							}
							else if (dogSort.Equals("GENDER"))
							{
								Console.WriteLine("Enter gender (M/F): ");
								char dogGender = Convert.ToChar(Console.ReadLine().ToUpper());

								IEnumerable<Animal> dogGenderCriteria = from Dog in animals.OfType<DogAnimal>()
																		where Dog.Gender == dogGender
																		select Dog;

								foreach(DogAnimal dogGenderCriteriaMet in dogGenderCriteria)
								{
									Console.WriteLine(dogGenderCriteriaMet);
								}
							}
							else
							{
								//Species
								Console.WriteLine("Enter species: ");
								string dogSpecies = Console.ReadLine().ToUpper();

								IEnumerable<Animal> dogSpeciesCriteria = from Dog in animals.OfType<DogAnimal>()
																		 where Dog.Species.ToUpper() == dogSpecies
																		 select Dog;

								foreach(DogAnimal dogSpeciesCriteriaMet in dogSpeciesCriteria)
								{
									Console.WriteLine(dogSpeciesCriteriaMet);
								}
							}

						}
						else
						{
							//Bird
							var birdAnimals = animals.OfType<BirdAnimal>();

							foreach (BirdAnimal birds in birdAnimals)
							{
								Console.WriteLine(birds);
							}

							Console.WriteLine("\nSort by: height, gender, species: ");
							string birdSort = Console.ReadLine().ToUpper();

							if (birdSort.Equals("HEIGHT"))
							{
								Console.WriteLine("Enter minimum height: ");
								int birdHeight = Convert.ToInt32(Console.ReadLine());

								IEnumerable<Animal> birdHeightCriteria = from Bird in animals.OfType<BirdAnimal>()
																   where Bird.Height >= birdHeight
																   select Bird;

								foreach(BirdAnimal birdHeightCriteriaMet in birdHeightCriteria)
								{
									Console.WriteLine(birdHeightCriteriaMet);
								}

							}else if (birdSort.Equals("GENDER"))
							{
								Console.WriteLine("Enter gender (M/F): ");
								char birdGender = Convert.ToChar(Console.ReadLine().ToUpper());

								IEnumerable<Animal> birdGenderCriteria = from Bird in animals.OfType<BirdAnimal>()
																		where Bird.Gender == birdGender
																		 select Bird;

								foreach (DogAnimal birdGenderCriteriaMet in birdGenderCriteria)
								{
									Console.WriteLine(birdGenderCriteriaMet);
								}
							}
							else
							{
								//Species
								Console.WriteLine("Enter species: ");
								string birdSpecies = Console.ReadLine().ToUpper();

								IEnumerable<Animal> birdSpeciesCriteria = from Bird in animals.OfType<BirdAnimal>()
																		 where Bird.Species.ToUpper() == birdSpecies
																		 select Bird;

								foreach (BirdAnimal birdSpeciesCriteriaMet in birdSpeciesCriteria)
								{
									Console.WriteLine(birdSpeciesCriteriaMet);
								}
							}

						}
						break;
					case 2:
						keepGoing = false;
						break;
					default:
						Console.WriteLine("That's not an option");
						break;
				}
			}
			
		}



	}
}

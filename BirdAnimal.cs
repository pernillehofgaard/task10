﻿namespace Task7
{
	public class BirdAnimal : Animal
	{
		private int wingSpanInCm;

		public BirdAnimal(string species, int height, string eyeColor, char Gender, int wingSpanInCm)
			:base(species, height, eyeColor, Gender)
		{
			this.wingSpanInCm = wingSpanInCm;
		}

		public override void Eat()
		{
			System.Console.WriteLine("Birdy nam nam");
		}

		public override void Sleep()
		{
			System.Console.WriteLine("Sleepy bird");
		}
	}
}
